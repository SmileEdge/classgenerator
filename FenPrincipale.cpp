//
// Created by costa on 09/01/2019.
//

#include <iostream>
#include "FenPrincipale.h"
#include "FenCodeGenere.h"

FenPrincipale::FenPrincipale()
{
    defClasse = new QGroupBox("Définition de la classe", this);

    header = new QString("");
    source = new QString("");

    nomClasse = new QLineEdit("Magicien");
    classeMere = new QLineEdit("Personnage");

    lNom = new QLabel("Nom :");
    lMere = new QLabel("Classe mère :");

    def = new QGridLayout();
    def->addWidget(lNom, 0, 0);
    def->addWidget(nomClasse, 0, 1);
    def->addWidget(lMere, 1, 0);
    def->addWidget(classeMere, 1, 1);

    defClasse->setLayout(def);

    options = new QGroupBox("Options");

    proteger = new QCheckBox("Protéger le header contre les inclusions multiples");
    proteger->setChecked(true);
    genCons = new QCheckBox("Générer un constructeur par défaut");
    genCons->setChecked(true);
    genDes = new QCheckBox("Générer un destructeur");
    genDes->setChecked(true);

    opt = new QVBoxLayout();
    opt->addWidget(proteger);
    opt->addWidget(genCons);
    opt->addWidget(genDes);

    options->setLayout(opt);

    ajoutCom = new QGroupBox("Ajouter des commentaires");
    ajoutCom->setCheckable(true);
    //ajoutCom->setChecked(false);

    auteur = new QLineEdit("Arnaud");
    date = new QDateEdit(QDate::currentDate());
    role = new QTextEdit("Oui");

    lAuteur = new QLabel("Auteur :");
    lDate = new QLabel("Date de création :");
    lRole = new QLabel("Rôle de la classe :");

    ajout = new QGridLayout();
    ajout->addWidget(lAuteur, 0, 0);
    ajout->addWidget(auteur, 0, 1);
    ajout->addWidget(lDate, 1, 0);
    ajout->addWidget(date, 1, 1);
    ajout->addWidget(lRole, 2, 0);
    ajout->addWidget(role, 2, 1);

    ajoutCom->setLayout(ajout);

    boutons = new QHBoxLayout();

    generer = new QPushButton("Générer");
    quitter = new QPushButton("Quitter");

    connect(quitter, SIGNAL(clicked()), qApp, SLOT(quit()));
    connect(generer, SIGNAL(clicked()), this, SLOT(ouvrirDialogue()));

    boutons->addWidget(generer);
    boutons->addWidget(quitter);

    groupeBoutons = new QFrame();
    groupeBoutons->setLayout(boutons);

    princ = new QVBoxLayout();
    princ->addWidget(defClasse);
    princ->addWidget(options);
    princ->addWidget(ajoutCom);
    princ->addWidget(groupeBoutons);

    this->setLayout(princ);
}

void FenPrincipale::ouvrirDialogue()
{
    classe = new QString(nomClasse->text());
    if(classe->isEmpty())
    {
        QMessageBox::critical(this, "Erreur", "Veuillez entrer au moins un nom de classe");
        return;
    }
    buildHeader();
    buildSource();
    FenCodeGenere dialogue(*header, *source, *classe);
    dialogue.exec();
    header = new QString("");
    source = new QString("");
}

void FenPrincipale::buildHeader()
{
    QTextStream stream(header, QIODevice::ReadWrite);
    stream.setCodec("UTF-16");
    if(ajoutCom->isChecked())
    {
        stream << "/*" << endl;
        if(!auteur->text().isEmpty())
        {
            stream << "Auteur : " << auteur->text() << endl;
        }
        stream << "Date de création : " << date->date().toString("dd/MM/yyyy") << endl;
        stream << "Rôle :" << endl << role->toPlainText() << endl;
        stream << "*/" << endl << endl << endl;
    }
    bool headerF = proteger->isChecked();
    if(headerF)
    {
        stream << "#ifndef HEADER_" << classe->toUpper() << endl;
        stream << "#define HEADER_" << classe->toUpper() << endl << endl << endl;
    }
    stream << "class " << *classe << " :";
    if(!classeMere->text().isEmpty())
        stream << " public " << classeMere->text();
    stream << endl << "{" << endl << "\tpublic:" << endl;
    if(genCons->isChecked())
        stream <<"\t" << *classe << "();" << endl;
    if(genDes->isChecked())
        stream << "\t~" << *classe << "();" << endl << endl;
    stream << "\tprotected:" << endl << endl << "\tprivate:" << endl << "};";
    if(headerF)
        stream << endl << endl << "#endif";
    //std::cout << header->toStdString();
}

void FenPrincipale::buildSource() {
    QTextStream stream(source, QIODevice::ReadWrite);
    stream.setCodec("UTF-16");
    if(ajoutCom->isChecked())
    {
        stream << "/*" << endl;
        if(!auteur->text().isEmpty())
        {
            stream << "Auteur : " << auteur->text() << endl;
        }
        stream << "Date de création : " << date->date().toString("dd/MM/yyyy") << endl;
        stream << "Rôle :" << endl << role->toPlainText() << endl;
        stream << "*/" << endl << endl << endl;
    }
    bool headerF = proteger->isChecked();
    if(headerF)
    {
        stream << "#ifndef HEADER_" << classe->toUpper() << endl;
        stream << "#define HEADER_" << classe->toUpper() << endl << endl << endl;
    }
    if(genCons->isChecked())
        stream << *classe << "::" << *classe << "()" << endl << "{}" << endl << endl;
    if(genDes->isChecked())
        stream << *classe << "::~" << *classe << "()" << endl << "{}" << endl << endl;
    if(headerF)
        stream << endl << endl << "#endif";
    //std::cout << source->toStdString();
}


