//
// Created by costa on 09/01/2019.
//

#ifndef CLASSGENERATORW_FENCODEGENERE_H
#define CLASSGENERATORW_FENCODEGENERE_H

#include <QtCore/QString>
#include <QtWidgets/QTabWidget>
#include <QtGui/QFont>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFileDialog>
#include <QtCore/QFile>

class FenCodeGenere : public QDialog {

    Q_OBJECT

    public:
        FenCodeGenere(const QString &header, const QString &source, const QString &classe);
    public slots:
        void ouvrirEnregister();
    private:

        QTabWidget *onglets;
        QWidget *boutons;

        QVBoxLayout *princL;
        QHBoxLayout *layoutBoutons;
        QTextEdit *codeHeader, *codeSource;
        QPushButton *fermer, *enregistrer;
        QString chemin, classe, header, source;

};


#endif //CLASSGENERATORW_FENCODEGENERE_H
