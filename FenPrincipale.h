//
// Created by costa on 09/01/2019.
//

#ifndef CLASSGENERATORW_FENPRINCIPALE_H
#define CLASSGENERATORW_FENPRINCIPALE_H


#include <QtWidgets>

class FenPrincipale : public QWidget {

    Q_OBJECT

    public:
        FenPrincipale();

    public slots:
        void ouvrirDialogue();

    private:
        QGroupBox *defClasse, *options, *ajoutCom;
        QFrame *groupeBoutons;

        QLineEdit *nomClasse, *classeMere;
        QLabel *lNom, *lMere;

        QCheckBox *proteger, *genCons, *genDes;

        QLineEdit *auteur;
        QDateEdit *date;
        QTextEdit *role;

        QLabel *lAuteur, *lDate, *lRole;

        QPushButton *generer, *quitter;

        QVBoxLayout *princ, *opt;
        QHBoxLayout *boutons;
        QGridLayout *def, *ajout;

        QString *header, *source, *classe;

        void buildHeader();
        void buildSource();

};


#endif //CLASSGENERATORW_FENPRINCIPALE_H
