//
// Created by costa on 09/01/2019.
//

#include <iostream>
#include <QtCore/QTextStream>
#include <QtWidgets/QMessageBox>
#include "FenCodeGenere.h"

FenCodeGenere::FenCodeGenere(const QString &header, const QString &source, const QString &classe)
{
    this->classe = classe;
    this->header = header;
    this->source = source;

    this->setMinimumSize(300, 500);

    onglets = new QTabWidget(this);

    codeHeader = new QTextEdit();
    codeHeader->setText(header);
    codeHeader->setPlainText(header);
    codeHeader->setReadOnly(true);
    codeHeader->setFont(QFont("Courier"));

    ///////////////////////////////////

    codeSource = new QTextEdit();
    codeSource->setText(source);
    codeSource->setPlainText(source);
    codeSource->setReadOnly(true);
    codeSource->setFont(QFont("Courier"));

    ///////////////////////////////////////
    
    onglets->addTab(codeHeader, "Header");
    onglets->addTab(codeSource, "Source");

    enregistrer = new QPushButton("Enregister");
    fermer = new QPushButton("Fermer");

    boutons = new QWidget();

    layoutBoutons = new QHBoxLayout();
    layoutBoutons->addWidget(enregistrer);
    layoutBoutons->addWidget(fermer);

    boutons->setLayout(layoutBoutons);

    princL = new QVBoxLayout();
    princL->addWidget(onglets);
    princL->addWidget(boutons);

    connect(fermer, SIGNAL(clicked()), this, SLOT(close()));
    connect(enregistrer, SIGNAL(clicked()), this, SLOT(ouvrirEnregister()));

    this->setLayout(princL);
}

void FenCodeGenere::ouvrirEnregister()
{
    chemin = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                               "/home",
                                               QFileDialog::DontResolveSymlinks);
    QFile headerFile(chemin+'/'+classe+".h"), sourceFile(chemin+'/'+classe+".cpp");
    int overwrite = 0;
    if(headerFile.exists() || sourceFile.exists())
    {
        QMessageBox fileExists(QMessageBox::Warning,
                "Attention",
                "Le ou les fichiers existent déjà, voulez-vous les remplacer ?",
                QMessageBox::Yes | QMessageBox::No);
        overwrite = fileExists.exec();
    }
    if(overwrite != QMessageBox::Yes)
        return;
    if (!headerFile.open(QIODevice::WriteOnly | QIODevice::Text) || !sourceFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox exception(QMessageBox::Warning, "Erreur", "Erreur lors de l'ouverture du ou des fichiers");
        exception.exec();
    }
    QTextStream outHeader(&headerFile);
    outHeader << this->header;
    headerFile.close();
    QTextStream outSource(&sourceFile);
    outSource << this->source;
    sourceFile.close();
}

